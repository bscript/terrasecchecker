# TerraSecChecker

TerraSecChecker is a Python-based utility that verifies the implementation of security controls in Terraform files. The tool checks for missing security controls by parsing the Terraform files and comparing the resources with defined rules for each resource type. Rules are stored in YAML files, making it easy for contributors to add or modify rules for various resource types like S3, EC2, ECS, VPC, and more.

TerraSecChecker supports the following features:

- [x] **Customizable Rules:** TerraSecChecker allows users to define custom security rules in YAML format. Each rule specifies a `name`, a `resource type`, and an optional `attribute` and `value` that should be present or absent in the resource definition. Users can define rules for multiple resource types and attributes.

- [x] **Related Resources:** TerraSecChecker also supports rules that require multiple resources to be present, for example, an EC2 instance must have an associated IAM instance profile. The tool can detect related resources based on naming conventions.

- [x] **Line Numbers:** When TerraSecChecker detects a security violation, it reports the line number in the Terraform file where the violation occurred. This makes it easy for users to locate and fix the issue.

- [x] **Extensible:** TerraSecChecker is designed to be extensible, so users can add new rules and customize the tool to fit their specific needs.

- [ ] Easy Integration: TerraSecChecker can be easily integrated into existing CI/CD pipelines. It can be run as a command-line tool, and it accepts a directory or a single file as input.

- [ ] Multiple Output Formats: TerraSecChecker supports multiple output formats, including plain text and JSON. Users can choose the format that best fits their needs.

## Prerequisites

- Python 3.6 or higher
- PyHCL (pip install pyhcl)
- PyYAML (pip install pyyaml)

## Installation

Clone the repository:

```
git clone https://gitlab.com/bscript/terrasecchecke.git
```
Change into the TerraSecChecker directory:

```
cd TerraSecChecker
```
Install the required Python packages:

```
pip3 install -r requirements.txt
```

## Usage

TerraSecChecker can scan either a single Terraform file or a directory containing multiple Terraform files. To use the tool, run the following command:

```
python3 terraform_security_check.py <terraform_file_or_directory> <rules_directory>
```

Replace `<terraform_file_or_directory>` with the path to your Terraform file or the directory containing multiple Terraform files, and `<rules_directory>` with the path to the directory containing the resource-specific rule files.

For example, to scan a single Terraform file:
```
python3 terraform_security_check.py main.tf rules
```
Or, to scan a directory containing multiple Terraform files:
```
python3 terraform_security_check.py terraform_files rules
```
TerraSecChecker will parse the specified Terraform files and compare the resources with the defined `rules` in the rules directory, reporting any missing security controls.

## Contributing

We welcome and encourage contributions to TerraSecChecker! You can contribute by adding or modifying rules for various resource types.

### Adding or modifying rules

1. Locate the appropriate YAML file for the resource type you want to add or modify rules for in the rules directory. If it doesn't exist, create a new file with the format `<resource_type>.yaml`.

2. Edit the YAML file to add or modify rules. Each rule should have a name, `resource_type`, and `related_to` key. The name describes the rule, the `resource_type` specifies the required resource, and the `related_to` key indicates the primary resource the rule applies to.

Example rule for an EC2 instance:
```
- name: "Require encrypted EBS volumes for EC2 instances"
  resource_type: "aws_ebs_volume"
  related_to: "aws_instance"
```
3. Submit a merge request with your changes.

## Donations

Your support helps us maintain and improve TerraSecChecker! If you find this tool useful, please consider donating. Any amount is greatly appreciated!

Thank you for your support!

