import hcl
import yaml
import sys
import os
import re
import glob

def load_rules(rule_files):
    all_rules = {}
    for rule_file in rule_files:
        with open(rule_file, "r") as f:
            rules = yaml.safe_load(f)
        resource_type = os.path.splitext(os.path.basename(rule_file))[0]
        all_rules[resource_type] = rules["rules"]
    return all_rules

def load_terraform_file(filename):
    with open(filename, "r") as f:
        content = f.read()
        tf = hcl.loads(content)

    line_numbers = {}
    with open(filename, "r") as f:
        lines = f.readlines()

        for idx, line in enumerate(lines):
            match = re.search(r'resource "(.*?)" "(.*?)"', line)
            if match:
                resource_type, resource_name = match.group(1), match.group(2)
                line_numbers[(resource_type, resource_name)] = idx + 1

    return tf, line_numbers

def check_security_controls(tf, line_numbers, all_rules):
    violations_found = False

    for resource_type, resource_instances in tf.get("resource", {}).items():
        rules = all_rules.get(resource_type, [])

        for resource_name, resource in resource_instances.items():
            resource_violations = []

            for rule in rules:
                required_resource_type = rule["resource_type"]
                related_resources = [
                    res for res_name, res in tf.get("resource", {}).get(required_resource_type, {}).items()
                    if res_name.startswith(resource_name + "_")
                ]

                if not related_resources:
                    line_number = line_numbers.get((resource_type, resource_name), -1)
                    resource_violations.append((rule["name"], line_number))

            if resource_violations:
                violations_found = True
                print(f"{resource_type} '{resource_name}' is missing the following security controls:")
                for violation, line_number in resource_violations:
                    print(f"- {violation} (line {line_number})")
                print()  # Add an empty line for better readability

    if not violations_found:
        print("All security controls are implemented.")

def main(terraform_file, rules_directory):
    tf, line_numbers = load_terraform_file(terraform_file)

    rule_files = glob.glob(os.path.join(rules_directory, "*.yaml"))
    all_rules = load_rules(rule_files)

    check_security_controls(tf, line_numbers, all_rules)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: python terraform_security_check.py <terraform_file> <rules_directory>")
        sys.exit(1)

    terraform_file = sys.argv[1]
    rules_directory = sys.argv[2]

    main(terraform_file, rules_directory)